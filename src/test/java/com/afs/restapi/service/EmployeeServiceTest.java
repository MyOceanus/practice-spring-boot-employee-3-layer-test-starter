package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.SalaryErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {

    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_throw_age_error_exception_when_insert_employee_given_age15_age68() {
        //given
        Employee employeeYoung = new Employee(1, "baby", 15, "male", 100);
        Employee employeeOld = new Employee(2, "oldBoy", 68, "male", 500);

        // when then

        Assertions.assertThrows(AgeErrorException.class,()->employeeService.getInsert(employeeYoung));
        Assertions.assertThrows(AgeErrorException.class,()->employeeService.getInsert(employeeOld));

    }

    @Test
    void should_no_call_repo_exception_when_insert_employee_given_age15_age68() {
        //given
        Employee employeeYoung = new Employee(1, "baby", 15, "male", 100);
        Employee employeeOld = new Employee(2, "oldBoy", 68, "male", 500);

        // when then

        Mockito.verify(employeeRepository,times(0)).insert(any());

    }
    @Test
    void should_salary_error_exception_when_insert_employee_given_age31_salary100() {
        Employee employeeLower = new Employee(1, "lower", 32, "male", 100);

        Assertions.assertThrows(SalaryErrorException.class,()->employeeService.getInsert(employeeLower));

    }

    @Test
    void should_return_employee_when_insert_employee_given_age31_salary25000() {
        Employee employeeRight = new Employee(1, "lower", 31, "male", 25000);
        when(employeeRepository.insert(any())).thenReturn(employeeRight);

        Assertions.assertEquals(employeeService.getInsert(employeeRight),employeeRight);
    }


    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule() {
        Employee employeeInsert = new Employee(1, "lower", 31, "male", 25000);
        Employee employeeReturn = new Employee(1, "lower", 31, "male", 25000);
        employeeReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeReturn);

        Employee employee = employeeService.getInsert(employeeInsert);

        assertTrue(employee.getStatus());
        verify(employeeRepository).insert(argThat(employeeToSave->{
                    assertTrue(employeeToSave.getStatus());
                    return true;
                }));
    }


    @Test
    void should_return_employee_with_status_false_when_delete_employee_given_a_employee_id() {
            employeeService.getDelete(1);
            verify(employeeRepository).delete(Mockito.eq(1));
    }

    @Test
    void should_return_employee_when_update_employee_given_a_employee_id_employee() {
        Employee employeeInsert = new Employee(1, "lower", 31, "male", 25000);
        Employee employeeInserted = new Employee(1, "lower", 31, "male", 3000);
        when(employeeRepository.findById(anyInt())).thenReturn(employeeInsert);


        employeeService.getUpdate(1, employeeInserted);

        Mockito.verify(employeeRepository).update(argThat(id->{
            assertEquals(1, id);
            return true;
        }),argThat(employee->{
            employee.equals(employeeInserted);
            return true;
        }
        ));
    }




}