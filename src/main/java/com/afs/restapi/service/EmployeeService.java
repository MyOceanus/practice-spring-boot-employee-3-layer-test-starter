package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.SalaryErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public List<Employee> getAll() {
        return employeeRepository.findAll().stream()
                .filter(Employee::getStatus)
                .collect(Collectors.toList());
    }

    public Employee getById(int id) {
        Employee employee = employeeRepository.findById(id);
        return getVerifyEmployee(employee);
    }

    private static Employee getVerifyEmployee(Employee employee) {
        if(employee.getStatus()){
            return employee;
        }
        return null;
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee getInsert(Employee employee) {
        if (employee.getAge()<18||employee.getAge()>65){
            throw new AgeErrorException();
        }else if (employee.getAge()>30&&employee.getSalary()<20000){
            throw new SalaryErrorException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee getUpdate(int id, Employee employee) {
        if (employeeRepository.findById(id)!=null && employeeRepository.findById(id).getStatus()){
            return employeeRepository.update(id, employee);
        }
        return employee;
    }

    public void getDelete(int id) {
        employeeRepository.delete(id);
    }
}