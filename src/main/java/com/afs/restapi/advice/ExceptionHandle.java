package com.afs.restapi.advice;


import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.SalaryErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(ClassNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionFomat notFoundException(Exception e){
        return new ExceptionFomat(HttpStatus.NOT_FOUND.value(),e.getMessage());
    }

    @ExceptionHandler(AgeErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionFomat ageErrorException(Exception e){
        return new ExceptionFomat(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }


    @ExceptionHandler(SalaryErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionFomat salaryErrorException(Exception e){
        return new ExceptionFomat(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }
}
